#!/bin/zsh
ls -l $1 | grep '^-' | rev | cut -d'.' -f1 | rev | cut -d' ' -f1 | sort | uniq > $1/types
sed '/^-r*/d' $1/types > $1/types1
for i in `cat $1/types1`
do
    mkdir $1/$i
done

for i in `cat $1/types1`
do
    cp $1/*.$i $1/$i
done
rm $1/types1 $1/types
