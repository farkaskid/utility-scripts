#!/bin/zsh
echo 'Acquire::http::proxy "http://kyadav:abc123@10.25.0.42:3128/";' > /etc/apt/apt.conf
echo 'Acquire::https::proxy "https://kyadav:abc123@10.25.0.42:3128/";' >> /etc/apt/apt.conf
echo 'Acquire::ftp::proxy "ftp://kyadav:abc123@10.25.0.42:3128/";' >> /etc/apt/apt.conf
echo 'Acquire::socks::proxy "socks://kyadav:abc123@10.25.0.42:3128/";' >> /etc/apt/apt.conf
